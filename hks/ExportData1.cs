﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace hks
{
    public static class ExportData
    {
        public static void WriteToFile(List<string> results, string filename)
        {
            using (StreamWriter outStr = new StreamWriter(filename, false))
            {
                foreach(var team in results)
                {
                    outStr.WriteLine(team);
                }
            }
        }
    }
}
