﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace hks
{
    public class get_file_data
    {

        public List<string> GetData()
        {
            string fileName = @"C:\source_files\holmenkoll_stafetten\hks\resultater_2017_stripped.txt";
            List<string> dat;
            using (StreamReader instr = File.OpenText(fileName))
            {
                dat = new List<string>();
                string neste = string.Empty;
                while ((neste = instr.ReadLine()) != null)
                {
                    dat.Add(neste);
                }
            }
            return dat;
        }
    }
}
