﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace hks
{
    public class ParseTeam
    {
        public List<string> Parse(List<string> inDat)
        {
            string team = string.Empty;
            List<string> teams = new List<string>();

            foreach ( var str in inDat)
            {
                string pattern1 = @"^(\d+)  \. (.*)(([0-9]{1,2}:){1,2}[0-9]{2})\((\d+)\)"; //Plass Lag tid tid plass

                string pattern2 = @"^(.+)([0-9]{1}:[0-9]{2}:[0-9]{2})|([0-9]{2}:[0-9]{2})\((\d+)\)";     //Navn tid tid plass

                string pattern3 = @"([0-9]{2}:[0-9]{2})\((\d+)\)";               //Tid plass

                string pattern4 = @"(.*)([0-9]{2}:[0-9]{2}) ([+-][0-9]{2}:[0-9]{2})"; //Navn tid diff

                string pattern5 = @"(^.+\s$|^\s*$)";

                Regex reg1 = new Regex(pattern1);
                Match match1 = Regex.Match(str, pattern1);

                Regex reg2 = new Regex(pattern2);
                Match match2 = Regex.Match(str, pattern2);

                Regex reg3 = new Regex(pattern3);
                Match match3 = Regex.Match(str, pattern3);

                Regex reg4 = new Regex(pattern4);
                Match match4 = Regex.Match(str, pattern4);

                Regex reg5 = new Regex(pattern5);
                Match match5 = Regex.Match(str, pattern5, RegexOptions.Multiline);

                var l1 = match1.Groups;
                var l2 = match2.Groups;
                var l3 = match3.Groups;
                var l4= match4.Groups;
                var l5= match5.Groups;

                if (l1.Count > 1)
                {
                    //New team
                    if (team != string.Empty)
                    {
                        teams.Add(team);
                        team = string.Empty;
                    }
                    for (int ix = 1; ix < l1.Count; ix++)
                        team = team + l1[ix] + ",";
                }
                else
                if (l2.Count > 1)
                {
                    for (int ix = 1; ix < l2.Count; ix++)
                        team = team + l2[ix] + ",";
                }
                else
                if (l3.Count > 1)
                {
                    for (int ix = 1; ix < l3.Count; ix++)
                        team = team + l3[ix] + ",";
                }
                else
                if (l4.Count > 1)
                {
                    for (int ix = 1; ix < l4.Count; ix++)
                        team = team + l4[ix] + ",";
                }
                else
                if (l5.Count > 1)
                {
                    for (int ix = 1; ix < l5.Count; ix++)
                        team = team + l5[ix];
                }

            }
            teams.Add(team);
            return teams;
        }
    }
}
